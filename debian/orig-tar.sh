#!/bin/sh -e

VERSION=$2
TAR=../maven-compiler-plugin_$VERSION.orig.tar.gz
DIR=maven-compiler-plugin-$VERSION
TAG=$(echo "maven-compiler-plugin-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export https://svn.apache.org/repos/asf/maven/plugins/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
